# Base Ruby on Rails 6 app

Got tired of recreating a base Ruby on Rails app everytime I needed to create a new app for a client.

# Uses the following:

- Ruby 3+
- Rails 6.1+
- Node 14.x+
- Npm 6.x+
- Yarn 1.x+
- Webpacker 5
- Pry 0.14

# The only thing that has changed is:

- Adding Bootstrap 5 and Popper.js (`application.js`, `application.scss`)
- Clean up in gem file (and add `pry`, `pry-byebug`, `pry-rails`)
- Comment out active_storage and active_mailbox.  Turned off routes for active_mailbox.
- Added `main_controller` and `index` action.
- Changed routes to `root` to `main#index`.

# If you use this as a base, make sure you change any private keys that may be stored in the application.